FROM node:14-alpine

VOLUME ["/usr/src/auth/keys"]

WORKDIR /usr/src/auth

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 3000

CMD ["npm", "start"]

