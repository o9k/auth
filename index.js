require('dotenv').config()

const logger = (process.env.NODE_ENV === 'local')
  ? { level: 'info', prettyPrint: true }
  : { level: 'info' }

const options = (require.main === module)
  ? { logger }
  : {}

const fastify = require('fastify')(options)

/** Register fastify plugins */
fastify.register(require('fastify-cors'))
fastify.register(require('fastify-helmet'))
fastify.register(require('fastify-compress'))

/** Register routes */
fastify.register(require('./lib/routes'))

/** Register plugins */
fastify.setErrorHandler(require('./lib/plugins/oops'))
fastify.setNotFoundHandler(require('./lib/plugins/nf'))

module.exports = (require.main === module)
  ? fastify.listen(3000, '0.0.0.0')
  : () => fastify
