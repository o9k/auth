resource "random_password" "database_root_password" {
  special = false
  length  = 64
}

resource "random_string" "database_database" {
  special = false
  length  = 16
}

resource "random_string" "database_username" {
  special = false
  length  = 16
}

resource "random_password" "database_password" {
  special = false
  length  = 64
}

resource "helm_release" "auth-mongo" {
  name       = "auth-mongo"
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "mongodb"

  set {
    name  = "architecture"
    value = "standalone"
  }

  set {
    name  = "persistence.storageClass"
    value = "do-block-storage"
  }

  set {
    name  = "persistence.accessMode"
    value = "ReadWriteOnce"
  }

  set {
    name  = "persistence.size"
    value = "1Gi"
  }

  set_sensitive {
    name  = "auth.rootPassword"
    value = random_password.database_root_password.result
  }

  set_sensitive {
    name  = "auth.username"
    value = random_string.database_username.result
  }

  set_sensitive {
    name  = "auth.password"
    value = random_password.database_password.result
  }

  set_sensitive {
    name  = "auth.database"
    value = random_string.database_database.result
  }
}

