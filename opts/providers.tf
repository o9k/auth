terraform {
  backend "remote" {
    organization = "o9k"

    workspaces {
      name = "auth"
    }
  }

  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "2.5.1"
    }

    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.0.2"
    }

    helm = {
      source  = "hashicorp/helm"
      version = "2.0.2"
    }

    random = {
      source  = "hashicorp/random"
      version = "3.0.1"
    }
  }
}

provider "digitalocean" {
  token = var.digitalocean_token
}

data "digitalocean_kubernetes_cluster" "o9k" {
  name = "o9k"
}


provider "kubernetes" {
  host  = data.digitalocean_kubernetes_cluster.o9k.endpoint
  token = data.digitalocean_kubernetes_cluster.o9k.kube_config[0].token
  cluster_ca_certificate = base64decode(
    data.digitalocean_kubernetes_cluster.o9k.kube_config[0].cluster_ca_certificate
  )
}

provider "helm" {
  kubernetes {
    host  = data.digitalocean_kubernetes_cluster.o9k.endpoint
    token = data.digitalocean_kubernetes_cluster.o9k.kube_config[0].token
    cluster_ca_certificate = base64decode(
      data.digitalocean_kubernetes_cluster.o9k.kube_config[0].cluster_ca_certificate
    )
  }
}

