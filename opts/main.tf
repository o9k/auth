resource "kubernetes_secret" "keys" {
  metadata {
    name = "keys"
  }

  data = {
    "rsa.key"     = file("${path.module}/rsa.key")
    "rsa.key.pub" = file("${path.module}/rsa.key.pub")
  }

  type = "Opaque"
}

resource "kubernetes_deployment" "auth" {
  depends_on = [helm_release.auth-mongo]

  metadata {
    name = "auth"
    labels = {
      app = "auth"
    }
  }

  spec {
    replicas = 2

    strategy {
      type = "RollingUpdate"
      rolling_update {
        max_surge       = "50%"
        max_unavailable = "50%"
      }
    }

    selector {
      match_labels = {
        app = "auth"
      }
    }

    template {
      metadata {
        labels = {
          app = "auth"
        }
      }

      spec {
        container {
          name              = "auth"
          image             = "registry.digitalocean.com/o9k/auth:0.4.1"
          image_pull_policy = "Always"

          port {
            container_port = "3000"
            host_port      = "3000"
          }

          liveness_probe {
            http_get {
              path = "/"
              port = 3000
            }
          }

          env {
            name  = "DATABASE_USERNAME"
            value = random_string.database_username.result
          }

          env {
            name  = "DATABASE_PASSWORD"
            value = random_password.database_password.result
          }

          env {
            name  = "DATABASE_DATABASE"
            value = random_string.database_database.result
          }

          env {
            name  = "DATABASE_HOST"
            value = "auth-mongo-mongodb"
          }

          env {
            name  = "DATABASE_PORT"
            value = "27017"
          }

          volume_mount {
            name       = "keys"
            mount_path = "/usr/src/auth/keys"
            read_only  = true
          }
        }

        volume {
          name = "keys"
          secret {
            secret_name = "keys"
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "auth" {
  depends_on = [kubernetes_deployment.auth]

  metadata {
    name = "auth"
  }

  spec {
    session_affinity = "ClientIP"
    selector = {
      app = "auth"
    }

    port {
      port        = 3000
      target_port = 3000
    }

    type = "NodePort"
  }
}

resource "kubernetes_ingress" "auth" {
  depends_on = [kubernetes_service.auth]

  metadata {
    name = "auth"
    annotations = {
      "kubernetes.io/ingress.class" = "nginx"
    }
  }

  spec {
    rule {
      # host = "auth.o9k.io"
      http {
        path {
          path = "/"
          backend {
            service_name = "auth"
            service_port = "3000"
          }
        }
      }
    }

    tls {
      hosts       = ["o9k.io", "*.o9k.io"]
      secret_name = "o9kio"
    }
  }
}

