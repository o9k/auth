const tfa = require('node-2fa')
const { test, tearDown } = require('tap')

const { version } = require('../package.json')

const fastify = require('../')()

test('notFoundHandler', async (t) => {
  const response = await fastify.inject({ method: 'GET', url: '/something/random' })
  const json = response.json()

  t.ok(response.statusCode, 200)

  t.ok(json.oops)
  t.equal(json.oops, 'this is not the path you\'re looking for')
})

test('get /', async (t) => {
  const response = await fastify.inject({ method: 'GET', url: '/' })
  const json = response.json()

  t.ok(response.statusCode, 200)

  t.ok(json.service)
  t.equal(json.service, 'auth')

  t.ok(json.version)
  t.equal(json.version, version)
})

test('get /key ok', async (t) => {
  const response = await fastify.inject({ method: 'GET', url: '/key' })
  const json = response.json()

  t.ok(response.statusCode, 200)
  t.ok(json.key)
})

test('post /users, ok', async (t) => {
  const username = 'testing'
  const password = 'testing'
  const base64 = Buffer.from(`${username}:${password}`).toString('base64')
  const headers = { authorization: `Basic ${base64}` }

  const response = await fastify.inject({ method: 'POST', url: '/users', headers })
  const json = response.json()

  t.ok(response.statusCode, 200)

  t.ok(json.message)
  t.equal(json.message, 'ok')
})

test('post /users, oops: user already exists', async (t) => {
  const username = 'testing'
  const password = 'testing'
  const base64 = Buffer.from(`${username}:${password}`).toString('base64')
  const headers = { authorization: `Basic ${base64}` }

  const response = await fastify.inject({ method: 'POST', url: '/users', headers })
  const json = response.json()

  t.ok(response.statusCode, 200)

  t.ok(json.oops)
  t.equal(json.oops, 'user already exists')
})

test('get /users/token, oops: invalid username', async (t) => {
  const username = 'wrong'
  const password = 'testing'
  const base64 = Buffer.from(`${username}:${password}`).toString('base64')
  const headers = { authorization: `Basic ${base64}` }

  const response = await fastify.inject({ method: 'GET', url: '/users/token', headers })
  const json = response.json()

  t.ok(response.statusCode, 200)
  t.ok(json.oops)

  t.equal(json.oops, 'invalid username')
})

test('get /users/token, oops: invalid password', async (t) => {
  const username = 'testing'
  const password = 'wrong'
  const base64 = Buffer.from(`${username}:${password}`).toString('base64')
  const headers = { authorization: `Basic ${base64}` }

  const response = await fastify.inject({ method: 'GET', url: '/users/token', headers })
  const json = response.json()

  t.ok(response.statusCode, 200)
  t.ok(json.oops)

  t.equal(json.oops, 'invalid password')
})

let token = ''
test('get /users/token, ok', async (t) => {
  const username = 'testing'
  const password = 'testing'
  const base64 = Buffer.from(`${username}:${password}`).toString('base64')
  const headers = { authorization: `Basic ${base64}` }

  const response = await fastify.inject({ method: 'GET', url: '/users/token', headers })
  const json = response.json()

  t.ok(response.statusCode, 200)
  t.ok(json.token)

  token = json.token
})

test('get /users, ok', async (t) => {
  const headers = { authorization: `Bearer ${token}` }

  const response = await fastify.inject({ method: 'GET', url: '/users', headers })
  const json = response.json()

  t.ok(response.statusCode, 200)
  t.ok(json.users)
})

test('delete /users, ok', async (t) => {
  const headers = { authorization: `Bearer ${token}` }

  const response = await fastify.inject({ method: 'DELETE', url: '/users', headers })
  const json = response.json()

  t.ok(response.statusCode, 200)
  t.ok(json.message)
})

/** TFA */

test('post /users, ok', async (t) => {
  const username = 'testing'
  const password = 'testing'
  const base64 = Buffer.from(`${username}:${password}`).toString('base64')
  const headers = { authorization: `Basic ${base64}` }

  const response = await fastify.inject({ method: 'POST', url: '/users', headers })
  const json = response.json()

  t.ok(response.statusCode, 200)

  t.ok(json.message)
  t.equal(json.message, 'ok')
})

test('get /users/:tfatoken/token, oops: tfa not active', async (t) => {
  const username = 'testing'
  const password = 'testing'
  const base64 = Buffer.from(`${username}:${password}`).toString('base64')
  const headers = { authorization: `Basic ${base64}` }

  const response = await fastify.inject({ method: 'GET', url: '/users/0000/token', headers })
  const json = response.json()

  t.ok(response.statusCode, 200)
  t.ok(json.oops)

  t.equal(json.oops, 'tfa not active')
})

test('get /users/token, ok', async (t) => {
  const username = 'testing'
  const password = 'testing'
  const base64 = Buffer.from(`${username}:${password}`).toString('base64')
  const headers = { authorization: `Basic ${base64}` }

  const response = await fastify.inject({ method: 'GET', url: '/users/token', headers })
  const json = response.json()

  t.ok(response.statusCode, 200)
  t.ok(json.token)

  token = json.token
})

let secret = ''
test('get /users/tfa, ok', async (t) => {
  const headers = { authorization: `Bearer ${token}` }

  const response = await fastify.inject({ method: 'GET', url: '/users/tfa', headers })
  const json = response.json()

  t.ok(response.statusCode, 200)
  t.ok(json.tfa)

  secret = json.tfa
})

test('get /users/:tfatoken/token, oops: invalid tfa', async (t) => {
  const username = 'testing'
  const password = 'testing'
  const base64 = Buffer.from(`${username}:${password}`).toString('base64')
  const headers = { authorization: `Basic ${base64}` }

  const response = await fastify.inject({ method: 'GET', url: '/users/0000/token', headers })
  const json = response.json()

  t.ok(response.statusCode, 200)
  t.ok(json.oops)

  t.equal(json.oops, 'invalid tfa')
})

test('get /users/:tfatoken/token, oops: invalid username', async (t) => {
  const username = 'wrong'
  const password = 'testing'
  const base64 = Buffer.from(`${username}:${password}`).toString('base64')
  const headers = { authorization: `Basic ${base64}` }

  const { token: tfatoken } = tfa.generateToken(secret)

  const response = await fastify.inject({ method: 'GET', url: `/users/${tfatoken}/token`, headers })
  const json = response.json()

  t.ok(response.statusCode, 200)
  t.ok(json.oops)

  t.equal(json.oops, 'invalid username')
})

test('get /users/:tfatoken/token, oops: invalid password', async (t) => {
  const username = 'testing'
  const password = 'wrong'
  const base64 = Buffer.from(`${username}:${password}`).toString('base64')
  const headers = { authorization: `Basic ${base64}` }

  const { token: tfatoken } = tfa.generateToken(secret)

  const response = await fastify.inject({ method: 'GET', url: `/users/${tfatoken}/token`, headers })
  const json = response.json()

  t.ok(response.statusCode, 200)
  t.ok(json.oops)

  t.equal(json.oops, 'invalid password')
})

test('get /users/token, oops: tfa is active', async (t) => {
  const username = 'testing'
  const password = 'testing'
  const base64 = Buffer.from(`${username}:${password}`).toString('base64')
  const headers = { authorization: `Basic ${base64}` }

  const response = await fastify.inject({ method: 'GET', url: '/users/token', headers })
  const json = response.json()

  t.ok(response.statusCode, 200)
  t.ok(json.oops)

  t.equal(json.oops, 'tfa is active')
})

test('get /users/:tfatoken/token, ok', async (t) => {
  const username = 'testing'
  const password = 'testing'
  const base64 = Buffer.from(`${username}:${password}`).toString('base64')
  const headers = { authorization: `Basic ${base64}` }

  const { token: tfatoken } = tfa.generateToken(secret)

  const response = await fastify.inject({ method: 'GET', url: `/users/${tfatoken}/token`, headers })
  const json = response.json()

  t.ok(response.statusCode, 200)
  t.ok(json.token)
})

test('delete /users/tfa, ok', async (t) => {
  const headers = { authorization: `Bearer ${token}` }

  const response = await fastify.inject({ method: 'DELETE', url: '/users/tfa', headers })
  const json = response.json()

  t.ok(response.statusCode, 200)
  t.ok(json.message)
})

test('delete /users, ok', async (t) => {
  const headers = { authorization: `Bearer ${token}` }

  const response = await fastify.inject({ method: 'DELETE', url: '/users', headers })
  const json = response.json()

  t.ok(response.statusCode, 200)
  t.ok(json.message)
})

tearDown(() => {
  fastify.close()
  process.exit(0)
})
