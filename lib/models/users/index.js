const { mongoose } = require('../../modules/mongoose')

const users = new mongoose.Schema({
  username: { type: String, required: true, index: true, unique: true },
  password: { type: String, required: true },

  tfa: { type: String, required: false }
}, {
  timestamps: { create: 'created_at', update: 'updated_at' },
  toJSON: { transform: (doc, ret) => { delete ret.password } }
})

const Users = mongoose.model('users', users)

module.exports = { Users, users }
