const fs = require('fs')
const path = require('path')

module.exports = (fastify, opts, done) => {
  fastify.get('/key', (request, reply) => {
    const key = fs.readFileSync(path.join(__dirname, '../../../keys/rsa.key.pub'))
    return reply.send({ key })
  })

  return done()
}
