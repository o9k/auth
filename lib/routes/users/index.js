const fs = require('fs')

const tfa = require('node-2fa')
const jwt = require('jsonwebtoken')

const { Users } = require('../../models/users')
const { verify, hash } = require('../../modules/hash')

const basic = ({ request }) => {
  const { authorization } = request.headers

  if (!authorization) {
    return { oops: 'missing authorization header' }
  }

  const regex = /^Basic (?<base64>(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?){1}$/
  const match = authorization.match(regex)

  if (!match?.groups?.base64) {
    return { oops: 'invalid authorization header' }
  }

  const base64 = Buffer.from(match.groups.base64, 'base64')
  const [username, password] = base64.toString('utf8').split(':')

  if (!password) {
    return { oops: 'invalid authorization token' }
  }

  return { username, password }
}

const bearer = ({ request }) => {
  const { authorization } = request.headers

  if (!authorization) {
    return { oops: 'missing authorization header' }
  }

  const regex = /Bearer (?<token>[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_.+/=]*)/
  const match = authorization.match(regex)

  if (!match?.groups?.token) {
    return { oops: 'invalid authorization header' }
  }

  const key = fs.readFileSync('./keys/rsa.key.pub')

  const token = jwt.verify(match.groups.token, key, { algorithms: ['RS256'] })

  if (!token) {
    return { oops: 'invalid jwt token' }
  }

  return { token }
}

module.exports = (fastify, opts, done) => {
  /** List users */
  fastify.get('/', async (request, reply) => {
    const { oops } = bearer({ request })
    if (oops) return reply.send({ oops })

    const users = await Users.find()
    return reply.send({ users })
  })

  /** Signup */
  fastify.post('/', async (request, reply) => {
    const { oops, username, password } = basic({ request })
    if (oops) return reply.send({ oops })

    if (await Users.findOne({ username })) {
      return reply.send({ oops: 'user already exists' })
    }

    await Users.create({ username, password: await hash(password) })
    return reply.send({ message: 'ok' })
  })

  fastify.get('/tfa', async (request, reply) => {
    const { oops, token } = bearer({ request })
    if (oops) return reply.send({ oops })

    const user = await Users.findOne({ _id: token.user._id })

    if (!user) {
      return reply.send({ oops: 'invalid user' })
    }

    if (user.tfa) {
      return reply.send({ oops: 'tfa already active' })
    }

    const secret = tfa.generateSecret({ name: 'o9k.io', account: user.username })
    user.tfa = secret.secret

    await user.save()

    return reply.send({ tfa: secret.secret })
  })

  fastify.delete('/tfa', async (request, reply) => {
    const { oops, token } = bearer({ request })
    if (oops) return reply.send({ oops })

    const user = await Users.findOne({ _id: token.user._id })

    if (!user) {
      return reply.send({ oops: 'invalid user' })
    }

    if (!user.tfa) {
      return reply.send({ oops: 'tfa not active' })
    }

    user.tfa = ''
    await user.save()

    return reply.send({ message: 'ok' })
  })

  /** Signin with tfa */
  fastify.get('/:tfatoken/token', async (request, reply) => {
    const { oops, username, password } = basic({ request })
    if (oops) return reply.send({ oops })

    const { tfatoken } = request.params

    const user = await Users.findOne({ username })

    if (!user) {
      return reply.send({ oops: 'invalid username' })
    }

    if (!(await verify(user.password, password))) {
      return reply.send({ oops: 'invalid password' })
    }

    if (!user.tfa) {
      return reply.send({ oops: 'tfa not active' })
    }

    const { delta } = tfa.verifyToken(user.tfa, tfatoken) || {}

    if (delta !== 0) {
      return reply.send({ oops: 'invalid tfa' })
    }

    const key = fs.readFileSync('./keys/rsa.key')
    const token = jwt.sign({ user }, { key, passphrase: '' }, { algorithm: 'RS256' })

    return reply.send({ token })
  })

  /** Signin */
  fastify.get('/token', async (request, reply) => {
    const { oops, username, password } = basic({ request })
    if (oops) return reply.send({ oops })

    const user = await Users.findOne({ username })

    if (!user) {
      return reply.send({ oops: 'invalid username' })
    }

    if (!(await verify(user.password, password))) {
      return reply.send({ oops: 'invalid password' })
    }

    if (user.tfa) {
      return reply.send({ oops: 'tfa is active' })
    }

    const key = fs.readFileSync('./keys/rsa.key')
    const token = jwt.sign({ user }, { key, passphrase: '' }, { algorithm: 'RS256' })

    return reply.send({ token })
  })

  /** Signout */
  fastify.delete('/', async (request, reply) => {
    const { oops, token } = bearer({ request })
    if (oops) return reply.send({ oops })

    const user = await Users.findOne({ _id: token.user._id })

    if (!user) {
      return reply.send({ oops: 'invalid user' })
    }

    await Users.deleteOne({ _id: token.user._id })

    return reply.send({ message: 'ok' })
  })

  return done()
}
