module.exports = (fastify, opts, done) => {
  fastify.register(require('./version'))
  fastify.register(require('./key'))

  fastify.register(require('./users'), { prefix: '/users' })
  return done()
}
