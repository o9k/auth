module.exports = (error, request, reply) => {
  request.log.error(error)
  return reply.status(200).send({ oops: 'something went wrong' })
}
