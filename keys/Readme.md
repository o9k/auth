The keys in this directory are for testing purpose,
those are not the one running on the service :D

Generate private key in PEM format

```bash
ssh-keygen -t rsa -b 4096 -m PEM -f auth0.key -C ""
```

Convert public key in PEM format

```bash
openssl rsa -in auth0.key -pubout -outform PEM -out auth0.key.pub
```

