### Documentation

The following documentation is specific for version 0.3.1

#### Service version

Retrieve service name and version

```bash
curl --header 'Accept: application/json' --request GET https://o9k.io
```

Response

```json
{
  "service": "auth",
  "version": "0.3.0"
}
```

#### Registration

Registration for new users

```bash
curl --header 'Accept: application/json' --header 'Authorization: Basic <base64(username:password)>' --request POST https://o9k.io/users
```

Response

```json
{
  "message": "ok"
}
```

### Development

Create your `.env` file

```bash
NODE_ENV=local
DATABASE_HOST=localhost
```

Run application

```bash
docker-compose run auth
```

Run test

```bash
docker-compose run test
```
